<#
.SYNOPSIS
    Download maps from SCMapDB.com
.DESCRIPTION
    Script will scrape the link for each page from http://scmapdb.com/tag:all, and then scrape all the map URIs from those pages, to then send to Wget to download
.PARAMETER DestinationDir
    Specify the directory in which to save the map files
.PARAMETER LogOutput
    Switch to log the wget output to $DestinationDir
.EXAMPLE
    # Download maps to E:\Games\SvenCoop\Maps and log output
    ./Download-SCMapDBMaps.ps1 -DestinationDir E:\Games\SvenCoop\Maps -LogOutput
.NOTES
    FileName:    Download-SCMapDBMaps.ps1
	Author:      Keenan Weaver
    Contact:     keenanweaver@pm.me
    Created:     2020-05-23
    Updated:     2020-05-23

    Version history:
    1.0.0 - (2020-05-23) Script created

    Prerequisites:
    ** Wget
    ** PowerShell (Tested on PowerShell 7)

    The intended use of this script is to be set up in a job scheduler like cron or Windows Task Scheduler.

    By default, this script will download to the current directory and retain the file structure of SCMapDB. If you wish to consolidate packs, edit the Wget parameters array with your appropriate parameters. For more info, see: https://www.gnu.org/software/wget/manual/wget.html
#>
[CmdletBinding()]
param(
    [Parameter()]
    [string]$DestinationDir,
    [Parameter()]
    [switch]$LogOutput
)
If (-not ($DestinationDir)) {
    $DestinationDir = $PSScriptRoot
}
# Set variables
$URI = "http://scmapdb.com/tag:all"
$Site = Invoke-WebRequest -Uri $URI
$Pages = $Site.Links.Href | Where-Object { $_ -match "/tag:all/p/" } | Sort-Object -Descending
$LatestPage = $Pages.Substring(11) | Measure-Object -Maximum | Select-Object -ExpandProperty Maximum
$ResultPages = @()
$MapPages = @()
$WgetParams = @(
    '-e',
    'robots=off',
    '-nH',
    '--no-cache',
    '-R ".DS_Store, Thumbs.db, thumbcache.db, desktop.ini, _macosx, index.html*"',
    '--random-wait',
    "-P `"$DestinationDir`"",
    '-U "Mozilla/5.0 (Linux x86_64; rv:77.0) Gecko/20100101 Firefox/77.0"',
    '-A "zip,rar,tar.gz,7z,exe"',
    '--no-parent',
    '-r',
    '-l 1',
    '--cut-dirs=2',
    '-w 2',
    '-nc',
    '-c'
)
If ($LogOutput) {
    $LogFile = Join-Path -Path $DestinationDir -ChildPath "WgetSCMapDB.log"
    $WgetParams += @(
        '-nv',
        "-a `"$LogFile`""
    )
}
## Assign page numbers
For ($num = 1; $num -le $LatestPage; $num++) {
    $ResultPages += $URI + "/p/" + $num
}

# Iterate pages and send to Wget
ForEach ($Page in $ResultPages) {
    $Junk = Invoke-WebRequest -Uri $Page
    $MapPages += $Junk.Links.Href | Where-Object { $_ -match "/map:" } | Select-Object -Unique | ForEach-Object { $URI.SubString(0, 18) + $_ }
    Clear-Variable Junk
}
$JoinedLinks += '"{0}"' -f ($MapPages -join '" "')

Start-Process -FilePath "wget" -ArgumentList "$WgetParams $JoinedLinks" -Wait