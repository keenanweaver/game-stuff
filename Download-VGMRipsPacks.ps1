<#
.SYNOPSIS
    Download zip archives from VGMRips.net
.DESCRIPTION
    Script will scrape the link for each page on https://vgmrips.net/packs/latest, and then scrape all the packs URIs from those pages, to then send to Wget to download
.PARAMETER DestinationDir
    Specify the directory in which to save the scraped pack files
.PARAMETER IncludeArt
    Switch to include downloading game pack art PNG files. Warning: This will cause execution time to dramatically increase as further iteration needs to be done to grab the image files.
.EXAMPLE
    # Download packs to E:\Music\VGMRips along with all art files.
    .\Download-VGMRipsPacks.ps1 -DestinationDir E:\Music\VGMRips -IncludeArt
.NOTES
    FileName:    Download-VGMRipsPacks.ps1
	Author:      Keenan Weaver
    Contact:     keenanweaver@pm.me
    Created:     2020-05-14
    Updated:     2020-05-14

    Version history:
    1.0.0 - (2020-05-14) Script created

    Prerequisites:
    ** Wget
    ** PowerShell (Tested on PowerShell 7)

    The intended use of this script is to be set up in a job scheduler like cron or Windows Task Scheduler.

    By default, this script will download to the current directory and retain the file structure of VGMRips. If you wish to consolidate packs, edit the Wget parameters array with your appropriate parameters. For more info, see: https://www.gnu.org/software/wget/manual/wget.html

    Shout-out to Wget helper site https://zget-opts-gen.now.sh/
#>
[CmdletBinding()]
param(
    [Parameter()]
    [string]$DestinationDir,
    [Parameter()]
    [switch]$IncludeArt
)
If (-not ($DestinationDir)) {
    $DestinationDir = "$PSScriptRoot"
}
# Set variables
$URI = "https://vgmrips.net/packs/latest"
$ProgressPreference = 'SilentlyContinue'
$Site = Invoke-WebRequest -Uri $URI
$Pages = $Site.Links.Href | Where-Object { $_ -match "\?p=" } | Select-Object -Unique
$IndexPages = @()
$PackArchives = @()
$GamePages = @()
$JoinedLinks = @()
$WgetParams = @(
    '-e',
    'robots=off',
    '-nH',
    '--no-cache',
    '-R ".DS_Store, Thumbs.db, thumbcache.db, desktop.ini, _macosx, index.html*"',
    '--random-wait',
    "-P $DestinationDir",
    '-U "Mozilla/5.0 (Linux x86_64; rv:77.0) Gecko/20100101 Firefox/77.0"',
    '-A "zip,rar,7z,png"',
    '--no-parent',
    '-r',
    '-l 1',
    '--cut-dirs=1',
    '-w 2',
    '-nc',
    '-c'
)
## Assign page numbers
For ($num = 0; $num -le $Pages.SubString(3)[-1]; $num++) {
    $IndexPages += $URI + "?p=" + $num
}

# Iterate pages
ForEach ($IndexPage in $IndexPages) {
    $PackPage = Invoke-WebRequest -Uri $IndexPage
    If ($IncludeArt) {
        $GamePages += $PackPage.Links.Href | Where-Object { ($_ -match "/pack/") -and ($_ -notmatch "#autoplay") } | Select-Object -Unique
        ForEach ($Page in $GamePages) {
            $GamePage = Invoke-WebRequest -Uri $Page
            $PackArchives += $GamePage.Links.Href | Where-Object { ($_ -match '\.(zip|png)$') } | Select-Object -Unique
        }
    }
    Else {
        $PackArchives += $PackPage.Links.Href | Where-Object { $_ -match '\.(zip)$' } | Select-Object -Unique
    }
    $JoinedLinks += '"{0}"' -f ($PackArchives -join '" "')
    Start-Process -FilePath "wget" -ArgumentList "$WgetParams $JoinedLinks" -Wait
    Clear-Variable -Name ("PackPage", "GamePages", "PackArchives", "JoinedLinks")
}