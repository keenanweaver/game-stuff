<#
.SYNOPSIS
    Download maps from RunThinkShootLive.com
.DESCRIPTION
    Script will scrape the link for each page from https://runthinkshootlive.com, and then scrape all the map URIs from those pages, to then send to Wget to download
.PARAMETER Game
    Select which game for which to download maps/mods. Possible values: All,HL,OF,HL2,HL2EP1,HL2EP2,BM
.PARAMETER DestinationDir
    Select the directory in which to download the files
.PARAMETER LogOutput
    Switch to log the wget output to $DestinationDir
.EXAMPLE
    # Download HL1 maps to E:\Games\HalfLife\Maps and log output
    ./Download-RTSL.ps1 -Game HL -DestinationDir E:\Games\HalfLife\Maps -LogOutput
.NOTES
    FileName:    Download-RTSL.ps1
	Author:      Keenan Weaver
    Contact:     keenanweaver@pm.me
    Created:     2020-05-24
    Updated:     2020-05-24

    Version history:
    1.0.0 - (2020-05-24) Script created

    Prerequisites:
    ** Wget
    ** PowerShell (Tested on PowerShell 7)

    The intended use of this script is to be set up in a job scheduler like cron or Windows Task Scheduler.

    By default, this script will download to the current directory and retain the file structure of SCMapDB. If you wish to consolidate packs, edit the Wget parameters array with your appropriate parameters. For more info, see: https://www.gnu.org/software/wget/manual/wget.html
#>
[CmdletBinding()]
param(
    [Parameter()]
    [string]$DestinationDir,
    [Parameter()]
    [switch]$LogOutput,
    [Parameter()]
    [ValidateSet("All", "HL", "OF", "HL2", "HL2EP1", "HL2EP2", "BM")]
    [string]$Game,
    [Parameter()]
    [switch]$DownloadMaps,
    [Parameter()]
    [switch]$DownloadMods
)
If (-not ($DestinationDir)) {
    $DestinationDir = $PSScriptRoot
}
# Set variables
Switch ($Game) {
    HL {
        $MapsURI = "https://www.runthinkshootlive.com/posts/category/play/half-life/hl-maps/"
        $ModsURI = "https://www.runthinkshootlive.com/posts/category/play/half-life/hl-mods/"
    }
    OF {
        $MapsURI = "https://www.runthinkshootlive.com/posts/category/play/opposing-force/of-maps/"
        $ModsURI = "https://www.runthinkshootlive.com/posts/category/play/opposing-force/of-mods/"
    }
}
$WgetParams = @(
    '-e',
    'robots=off',
    '-nH',
    '--no-cache',
    '-R ".DS_Store, Thumbs.db, thumbcache.db, desktop.ini, _macosx, index.html*"',
    '--random-wait',
    '-U "Mozilla/5.0 (Linux x86_64; rv:77.0) Gecko/20100101 Firefox/77.0"',
    '-A "zip,rar,7z,exe"',
    '--no-parent',
    '-w 2',
    '-nd',
    '-nc'
    '-c'
)
If ($LogOutput) {
    $LogFile = Join-Path -Path $DestinationDir -ChildPath "WgetRTSL.log"
    $WgetParams += @(
        '-nv',
        "-a `"$LogFile`""
    )
}
If ($DownloadMaps) {
    $ResultPagesMaps = @()
    $MapPages = @()
    $SiteMaps = Invoke-WebRequest -Uri $MapsURI
    $PagesMaps += @($SiteMaps.Links.Href | Where-Object { $_ -match "/page/" } | Select-Object -Unique)
    $LatestPageMaps = ($PagesMaps | Sort-Object)[0]
    $LatestPageNumMaps = ($LatestPageMaps -split "/")[9]
    ## Assign page numbers
    For ($num = 1; $num -le $LatestPageNumMaps; $num++) {
        $ResultPagesMaps += $MapsURI + "page/" + "$num/"
    }
    # Iterate pages and send to Wget
    ForEach ($Page in $ResultPagesMaps) {
        $Junk = Invoke-WebRequest -Uri $Page
        $MapPages += $Junk.Links.Href | Where-Object { ($_ -match "https://www.runthinkshootlive.com/posts/") -and ($_ -notmatch "comment") -and ($_ -notmatch "category") } | Select-Object -Unique
        Clear-Variable Junk
        ForEach ($Map in $MapPages) {
            $Junk = Invoke-WebRequest -Uri $Map
            $MapFileName = Split-Path ($Junk.Links.Href | Select-Object -Unique | Where-Object { ($_ -match "download.php") -and ($_ -notmatch "maptap:") }) -Leaf
            $MapLink = "https://www.runthinkshootlive.com" + ($Junk.Links.Href | Select-Object -Unique | Where-Object { ($_ -match "download.php") -and ($_ -notmatch "maptap:") }) -replace "#038;", ""
            $OutputFile = Join-Path -Path $DestinationDir -ChildPath $MapFileName
            Start-Process -FilePath "wget" -ArgumentList "$WgetParams -O `"$OutputFile`" $MapLink" -Wait
            Clear-Variable -Name ("Junk", "MapFileName", "MapLink")
        }
        Clear-Variable MapPages
    }
    Clear-Variable -Name ("ResultPagesMaps", "MapPages", "SiteMaps", "PagesMaps", "LatestPageMaps", "LatestPageNumMaps", "ResultPagesMaps")
}
If ($DownloadMods) {
    $ResultPagesMods = @()
    $ModPages = @()
    $SiteMods = Invoke-WebRequest -Uri $ModsURI
    $PagesMods += @($SiteMods.Links.Href | Where-Object { $_ -match "/page/" } | Select-Object -Unique)
    $LatestPageMods = ($PagesMods | Sort-Object)[0]
    $LatestPageNumMods = ($LatestPageMods -split "/")[9]
    ## Assign page numbers
    For ($num = 1; $num -le $LatestPageNumMods; $num++) {
        $ResultPagesMods += $ModsURI + "page/" + "$num/"
    }
    # Iterate pages and send to Wget
    ForEach ($Page in $ResultPagesMods) {
        $Junk = Invoke-WebRequest -Uri $Page
        $ModPages += $Junk.Links.Href | Where-Object { ($_ -match "https://www.runthinkshootlive.com/posts/") -and ($_ -notmatch "comment") -and ($_ -notmatch "category") } | Select-Object -Unique
        Clear-Variable Junk
        ForEach ($Mod in $ModPages) {
            $Junk = Invoke-WebRequest -Uri $Mod
            $ModFileName = Split-Path ($Junk.Links.Href | Select-Object -Unique | Where-Object { ($_ -match "download.php") -and ($_ -notmatch "maptap:") }) -Leaf
            $ModLink = "https://www.runthinkshootlive.com" + ($Junk.Links.Href | Select-Object -Unique | Where-Object { ($_ -match "download.php") -and ($_ -notmatch "maptap:") }) -replace "#038;", ""
            $OutputFile = Join-Path -Path $DestinationDir -ChildPath $ModFileName
            Start-Process -FilePath "wget" -ArgumentList "$WgetParams -O `"$OutputFile`" $ModLink" -Wait
            Clear-Variable -Name ("Mod", "Junk", "ModFileName", "ModLink")
        }
        Clear-Variable ModPages
    }
    Clear-Variable -Name ("ResultPagesMods", "ModPages", "SiteMods", "PagesMods", "LatestPageMods", "LatestPageNumMods", "ResultPagesMods")
}
