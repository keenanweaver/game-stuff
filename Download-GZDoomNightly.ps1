<#
.SYNOPSIS
    Download GZDoom development builds from https://devbuilds.drdteam.org/gzdoom/
.DESCRIPTION
    Script will scrape download link for the latest GZDoom development build from https://devbuilds.drdteam.org/gzdoom/, and, optionally, expand the archive to a directory.
.PARAMETER 7ZipExe
    Specify the 7Zip executable if it is a portable install.
.PARAMETER BuildVersion
    Specify which build to download. Select between: x64, x86, and All
.PARAMETER DownloadPath
    Specify the path to download the GZDoom build
.PARAMETER DownloadSource
    Specify the source from which to download the GZDoom build
.PARAMETER ExtractAsFolder
    Specify to extract GZDoom build as a folder. It will be a sub-folder to the specified folder in InstallPath.
.PARAMETER InstallPath
    Specify the containing path to expand the GZDoom build
.PARAMETER RemoveAfterExpand
    Specify to delete archive after expanding
.PARAMETER RemoveLegacyVersions
    Specify to delete old GZDoom build downloads
.NOTES
  Version:        1.0
  Author:         Keenan Weaver
  Creation Date:  02/16/2020
  Purpose/Change: Initial script development

.EXAMPLE
  Download-GZDoomDev.ps1 -BuildVersion x64 -DownloadPath D:\Doom\Ports\GZDoom -InstallPath D:\Doom\GZDoom
#>

[CmdletBinding()]
param(
    [Parameter(Mandatory)]
    [ValidateSet('x64', 'x86', 'All')]
    [string]$BuildVersion,
    [Parameter(Mandatory)]
    [string]$DownloadPath,
    [Parameter()]
    [string]$DownloadSource,
    [Parameter()]
    [string]$7ZipExe,
    [Parameter()]
    [string]$InstallPath,
    [Parameter()]
    [switch]$ExtractAsFolder,
    [Parameter()]
    [switch]$RemoveAfterExpand,
    [Parameter()]
    [switch]$RemoveLegacyVersions
)

# Define functions
function Get-GZDoomBuild {
    If (-not ($DownloadSource)) {
        $global:URI = "https://devbuilds.drdteam.org/gzdoom/"
    }
    function Start-GZDoomDownload {
        Invoke-WebRequest -Uri $URI | ForEach-Object { $_.Links.Href } | Where-Object { ($_ -match '.7z$') -and ($_ -match $BuildVersion) } | Select-Object -First 1 | ForEach-Object {
            If ($_ -match '^\/') {
                $global:DownloadLink = $URI + $_.Substring(8)
            }

            # Correct filename
            If ($IsWindows) {
                If (-not ($($DownloadPath)).EndsWith('\')) {
                    $DownloadPath = $DownloadPath + '\'
                }
            }
            ElseIf ($IsLinux -or $IsMacOS) {
                If (-not ($($DownloadPath)).EndsWith('/')) {
                    $DownloadPath = $DownloadPath + '/'
                }
            }

            $global:DownloadFileName = $DownloadLink.Substring($DownloadLink.LastIndexOf('/') + 1)
            $global:TempDownloadPath = $DownloadPath + $DownloadFileName

            (New-Object -TypeName System.Net.WebClient).DownloadFile($DownloadLink, $TempDownloadPath)
        }
    }

    If ($BuildVersion -eq "All") {
        "x64", "x86" | ForEach-Object {
            $BuildVersion = $_
            Start-GZDoomDownload
        }
    }
    Else {
        Start-GZDoomDownload
    }
}
function Expand-GZDoomBuild {
    If ($ExtractAsFolder) {
        If ($IsWindows) {
            $DirChar = "\"
        }
        Else {
            $DirChar = "/"
        }
        $InstallPath = $InstallPath + $DirChar + $DownloadFileName.Substring(0, $DownloadFileName.Length - 3)
    }

    If ($IsWindows) {
        If (-not ($7ZipExe)) {
            $7ZipExe = (Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\* | Where-Object { $_.DisplayName -like "7-Zip*" } | Select-Object -ExpandProperty InstallLocation) + "7z.exe"
            If ($7ZipExe -contains "7z.exe") {
                If ($IsWindows) {
                    $7ZipExe = "$env:ProgramFiles\7-Zip\7z.exe"
                }
            }
        }

        Get-ChildItem -Path $DownloadPath -File | Where-Object { $_.Name -match $DownloadFileName } | Select-Object -First 1 | ForEach-Object {
            Start-Process -FilePath "$7ZipExe" -ArgumentList "x `"$($_.FullName)`" -o`"$InstallPath`" -aoa" -WindowStyle Hidden
        }
    }
}

function Remove-GZDoomBuild {
    If ($RemoveAfterExpand) {
        Remove-Item -Path $TempDownloadPath -Force
    }
    If ($RemoveLegacyVersions) {
        $LegacyVersions = @(Get-ChildItem -Path $DownloadPath | Where-Object { $_.Name -match 'gzdoom-x(64|86)-g[0-9].[0-9]pre-[0-9]{2}-g[0-9a-zA-Z]*.7z' })
        ForEach ($Version in $LegacyVersions) {
            If ($Version -notmatch $DownloadFileName) {
                Remove-Item -Path $Version -Force
            }
        }
    }
}

# Execute functions
Get-GZDoomBuild
If ($InstallPath) {
    Start-Sleep -Seconds 3
    Expand-GZDoomBuild
}
If ($RemoveLegacyVersions -or $RemoveAfterExpand) {
    Remove-GZDoomBuild
}
